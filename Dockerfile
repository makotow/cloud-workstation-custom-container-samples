
# syntax=docker/dockerfile:1
# check=error=true

FROM us-central1-docker.pkg.dev/cloud-workstations-images/predefined/code-oss:latest

COPY scripts/300_python.sh /etc/workstation-startup.d/
COPY scripts/400_settings.sh /etc/workstation-startup.d/
COPY ./settings.json /etc/workstation-startup.d/

RUN chmod +x /etc/workstation-startup.d/300_python.sh
RUN chmod +x /etc/workstation-startup.d/400_settings.sh

## Conda
# Install our public GPG key to trusted store
RUN curl https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc | gpg --dearmor > conda.gpg
RUN install -o root -g root -m 644 conda.gpg /usr/share/keyrings/conda-archive-keyring.gpg

# Check whether fingerprint is correct (will output an error message otherwise)
RUN gpg --keyring /usr/share/keyrings/conda-archive-keyring.gpg --no-default-keyring --fingerprint 34161F5BF5EB1D4BFBBB8F0A8AEB4F8B29D82806

# Add our Debian repo
RUN echo "deb [arch=amd64 signed-by=/usr/share/keyrings/conda-archive-keyring.gpg] https://repo.anaconda.com/pkgs/misc/debrepo/conda stable main" > /etc/apt/sources.list.d/conda.list

# **NB:** If you receive a Permission denied error when trying to run the above command (because `/etc/apt/sources.list.d/conda.list` is write protected), try using the following command instead:
# echo "deb [arch=amd64 signed-by=/usr/share/keyrings/conda-archive-keyring.gpg] https://repo.anaconda.com/pkgs/misc/debrepo/conda stable main" | sudo tee -a /etc/apt/sources.list.d/conda.list

RUN sudo apt update && sudo apt install conda
