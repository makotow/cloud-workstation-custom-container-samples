#!/usr/bin/env bash

docker buildx build --check . 

if [ $? -eq 0 ]; then
    docker buildx build --platform linux/amd64 -t ${REGION}-docker.pkg.dev/${PROJECT_ID}/${IMAGE_NAME} .
fi
