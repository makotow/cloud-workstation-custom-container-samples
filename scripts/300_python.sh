#!/bin/bash
DEFAULT_USER=user
# https://conda.io/projects/conda/en/latest/user-guide/install/rpm-debian.html
runuser "${DEFAULT_USER}" -c  "echo \"source /opt/conda/etc/profile.d/conda.sh\" >> /home/${DEFAULT_USER}/.bashrc"

runuser "${DEFAULT_USER}" -c "
/opt/code-oss/bin/codeoss-cloudworkstations \
--install-extension ms-python.python ms-python.debugpy \
--install-extension ms-toolsai.jupyter \
--install-extension eamodio.gitlens \
--install-extension ms-python.flake8 \
--install-extension ms-python.black-formatter
"

## Example: conda environment creation.
runuser -l "${DEFAULT_USER}" -c "
source /opt/conda/etc/profile.d/conda.sh && \
conda create -n test1 python=3.10 && \
conda activate test1 && \
conda install conda-forge::lightgbm==4.3.0 -y && \
pip install --upgrade --force-reinstall pip && \
pip install asttokens==2.4.1 && \
pip install attrs==24.2.0 && \
pip install beautifulsoup4==4.12.3 && \
pip install certifi==2024.7.4 && \
pip install charset-normalizer==3.3.2 && \
pip install comm==0.2.2 && \
pip install contourpy==1.2.1 && \
pip install cramjam==2.8.3 && \
pip install cycler==0.12.1 && \
pip install debugpy==1.8.5 && \
pip install decorator==5.1.1 && \
pip install exceptiongroup==1.2.2 && \
pip install exchange_calendars==4.5.5 && \
pip install executing==2.0.1 && \
pip install fastparquet==2024.5.0 && \
pip install filelock==3.13.1 && \
pip install fonttools==4.53.1 && \
pip install frozendict==2.4.4 && \
pip install fsspec==2024.6.1 && \
pip install future==1.0.0 && \
pip install html5lib==1.1 && \
pip install hurst==0.0.5 && \
pip install idna==3.7 && \
pip install ipykernel==6.29.5 && \
pip install ipython==8.26.0 && \
pip install ipython-genutils==0.2.0 && \
pip install jedi==0.19.1 && \
pip install Jinja2==3.1.3 && \
pip install joblib==1.4.2 && \
pip install jsonschema==4.23.0 && \
pip install jsonschema-specifications==2023.12.1 && \
pip install jupyter_client==8.6.2 && \
pip install jupyter_core==5.7.2 && \
pip install kiwisolver==1.4.5 && \
pip install korean-lunar-calendar==0.3.1 && \
pip install lxml==5.3.0 && \
pip install MarkupSafe==2.1.5 && \
pip install matplotlib==3.9.2 && \
pip install matplotlib-inline==0.1.7 && \
pip install mplfinance==0.12.10b0 && \
pip install mpmath==1.3.0 && \
pip install multitasking==0.0.11 && \
pip install nbformat==5.1.2 && \
pip install nest-asyncio==1.6.0 && \
pip install networkx==3.2.1 && \
pip install numpy==1.24.4 && \
pip install numpy-indexed==0.3.7 && \
pip install packaging==24.1 && \
pip install pandas==2.2.2 && \
pip install pandas_market_calendars==4.4.1 && \
pip install parso==0.8.4 && \
pip install peewee==3.17.6 && \
pip install pexpect==4.9.0 && \
pip install pillow==10.4.0 && \
pip install platformdirs==4.2.2 && \
pip install plotly==5.23.0 && \
pip install prompt_toolkit==3.0.47 && \
pip install psutil==6.0.0 && \
pip install ptyprocess==0.7.0 && \
pip install pure_eval==0.2.3 && \
pip install pyarrow==17.0.0 && \
pip install Pygments==2.18.0 && \
pip install pyluach==2.2.0 && \
pip install pyparsing==3.1.2 && \
pip install python-dateutil==2.9.0.post0 && \
pip install pytz==2024.1 && \
pip install PyYAML==6.0.1 && \
pip install pyzmq==26.1.0 && \
pip install referencing==0.35.1 && \
pip install requests==2.32.3 && \
pip install rpds-py==0.20.0 && \
pip install scikit-learn==1.5.1 && \
pip install scipy==1.14.0 && \
pip install seaborn==0.13.2 && \
pip install six==1.16.0 && \
pip install soupsieve==2.5 && \
pip install stack-data==0.6.3 && \
pip install sympy==1.12 && \
pip install tenacity==9.0.0 && \
pip install threadpoolctl==3.5.0 && \
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118 && \
pip install toolz==0.12.1 && \
pip install tornado==6.4.1 && \
pip install tqdm==4.66.4 && \
pip install traitlets==5.14.3 && \
pip install triton==3.0.0 && \
pip install typing_extensions==4.12.2 && \
pip install tzdata==2024.1 && \
pip install urllib3==2.2.2 && \
pip install wcwidth==0.2.13 && \
pip install webencodings==0.5.1 && \
pip install yfinance==0.2.41 
"
