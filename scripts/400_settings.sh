#!/bin/bash
DEFAULT_USER=user
SETTINGS_FILE=/home/"${DEFAULT_USER}"/.codeoss-cloudworkstations/data/Machine/settings.json

sudo -i -u "${DEFAULT_USER}" cp -f /etc/workstation-startup.d/settings.json "${SETTINGS_FILE}"
