#!/usr/bin/env bash

docker run --privileged -d -p 8080:80 --rm ${REGION}-docker.pkg.dev/${PROJECT_ID}/${IMAGE_NAME} 